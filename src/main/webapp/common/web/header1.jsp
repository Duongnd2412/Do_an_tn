<%@ page pageEncoding="UTF-8" %>
<div style="background-color: #f2f2f2">
    <div class="container-fluid row" >
        <div class="col-md-4 col-sm-12 img-header">
            <c:forEach var="item" items="${infor}">
                <a class="navbar-brand" href="<c:url value="/trang-chu"/>" ><img class="img-header" src="${item.logo}"></a>
            </c:forEach>
        </div>
        <div class="col-md-3 col-sm-12"></div>
        <div class="col-md-4 col-sm-12" style="margin-top: 50px">
            <form class="form-inline" action="/search-new">
                <input class="form-group mr-3 w-75" name="title" type="text" placeholder="Search"
                       aria-label="Search">
                <button title="Tìm kiếm" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light nav-header">
        <div class="container">
            <a href="<c:url value="/trang-chu"/>"  class="btn btn-primary btn-b" style="background-color: #f02904"
                    aria-haspopup="true" aria-expanded="false" type="button" >
                <i class="fa fa-home"></i>
            </a>
            <c:forEach items="${group}" var="item">
                <div  class="dropdown" style="position: relative">
                    <a  href="/bai-viet/nhom/the-loai/${item.id}"class="btn btn-primary dropdown-toggle padding btn-b" style="background-color: #f02904"
                            aria-haspopup="true" aria-expanded="false" type="button" id="navbarDropdown${item.id}"  data-toggle="dropdown">${item.name}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown${item.id}" style="background-color: #ececec">
                        <c:forEach var="items" items="${item.categoryName}" varStatus="loop">
                            <a class="dropdown-item" href="/bai-viet/the-loai/${item.ids[loop.index]}" >${items}</a>
                        </c:forEach>
                    </div>
                </div>
            </c:forEach>
            <a href="#" class="btn btn-primary btn-b" style="background-color: #f02904"
               aria-haspopup="true" aria-expanded="false" type="button" >
                <b>VIDEO</b>
            </a>
            <a href="/feedback" class="btn btn-primary btn-b" style="background-color: #f02904"
                    aria-haspopup="true" aria-expanded="false" type="button" >
                <b>GÓP Ý</b>
            </a>
        </div>
    </nav>
</div>

