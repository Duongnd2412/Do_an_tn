var MIN_LENGTH = 1;
var MAX_LENGTH = 255;

var validationFields = [];

var validation = {
  init: function(fields) {
    validationFields = fields;

    this.generate();
  },
  generate: function() {
    for (var field of validationFields) {
      var fieldElement = this.getElement(field.selector);

    }
  },
  checkAllError: function() {
      var isError = false;


    for (var field of validationFields) {
      this.checkFieldError(field) && (isError = true);
    }
    return isError;
  },

  checkFieldError: function(field) {
      var fieldElement = this.getElement(field.selector);
      var valid = this[field.type](fieldElement.value, field.min, field.max);
    if (!valid) {
      return true;
    }
    return false;
  },
  noError: function() {
      var haveError = this.checkAllError();
    return !haveError;
  },
  text: function (value, min = MIN_LENGTH, max = MAX_LENGTH) {
      var length = value.length;
    return length >= min && length <= max;
  },
  number: function(value, min =MIN_LENGTH, max = MAX_LENGTH) {
      var isNumber = !!value && !isNaN(value);
      var length = value.length;
    return isNumber && length >= min && length <= max;
  },
  email: function(value) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
  },
  getElement: function(selector) {
      var element = document.querySelector(selector);
    return element;
  }
}
