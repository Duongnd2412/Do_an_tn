<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/common/taglib.jsp"%>
<c:url var="urlFeedback" value="/feedback"/>
<c:url var="urlShare" value="/feedback/api/create"/>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>feedback</title>
</head>
<body>

<div class="container" style="height: 65%">
    <div class="card card-login mx-auto mt-5"  style="width: 700px">
        <form id="formSubmit" method="post">
            <div class="form-group">
                <label>Email của bạn:</label>
                <input type="email" required class="form-control email" name="email" id="email" placeholder="name@gmail.com">
            </div>
            <div class="form-group">
                <label>Góp ý của bạn:</label>
                <textarea style="height: 150px" name="content" class="form-control" id="content" rows="3" required></textarea>
            </div>
            <button id="btnShare" class="btn btn-primary pull-left contents" type="submit">Gửi</button>
        </form>
    </div>
</div>

<script src="/js/validation.js"></script>
<script>
    document.addEventListener('DOMContentLoaded', function() {
        validation.init([
            {
                selector: '.email',
                name: 'email',
                type: 'email'
            }
        ]);
    });

    $('#btnShare').click(function (e) {
        if (!validation.noError()) return;
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        share(data);
    });

    function share(data) {

        $.ajax({
            url: '${urlShare}',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlFeedback}?feedback_success";
                alert('Gửi thành công!');
            },
            error: function () {
                window.location.href = "${urlFeedback}?false";
            }
        });
    }

</script>
</body>
</html>
