<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<c:url var="urlNew" value="/bai-viet/chi-tiet/${news.id}"/>
<c:url var="urlShare" value="/comment/create"/>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>${news.title}</title>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>


</head>

<body>


<!-- Page Content -->
<div class="container" >
    <div class="row" style="width: 1300px;">
        <div class="col-sm-3">
            <h1 class="my-4"></h1>
            <div class="list-group nav-link" style="background-color: #f9a057;">
                <a style="color: #f02904;"><b>CHUYÊN MỤC</b></a>
                <ul>
                    <c:forEach items="${group}" var="item">
                        <a href="/bai-viet/nhom/the-loai/${item.id}">${item.name}</a>
                        <hr>
                        <ul>
                            <c:forEach items="${item.categoryName}" var="item1" varStatus="loop">
                                <li>
                                    <a href="/bai-viet/the-loai/${item.ids[loop.index]}">
                                        <div class="clearfix">
                                                ${item1}
                                        </div>
                                    </a>
                                    <hr>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-sm-9 mt-3">
            <div style="width: 900px;">
                <h2>${news.title}</h2>
                <label class="news-date"><fmt:formatDate type = "both"
                                 dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${news.createdDate}" />
                </label>
                <hr>
                <div class="col-sm-12 col-md-12" id="content">
                    ${news.content}
                </div>
                <div >
                    <label class="view">
                        <i class="fa fa-eye"></i>Lượt xem: ${news.count}
                    </label>
                </div>
            </div>
            <hr>
            <div class="container pb-cmnt-container">
                <div class="row">
                    <div class="col-md-10">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <form id="formSubmit" action="/comment/create" method="post">
                                    <input name="name" id="name" style="width: 50%" placeholder="What is your name?">
                                    <textarea name="comment" id="comment" placeholder="Write your comment here!" class="pb-cmnt-textarea"></textarea>
                                    <button id="btnShare" class="btn btn-primary pull-left" type="submit">Share</button>
                                    <input type="hidden" name="newId" value="${news.id}"/>
                                </form>

                            </div>
                        </div>
                        <div style="margin-top: 50px">
                            <c:forEach items="${comments}" var="item">
                                <span style="color: #1b33cc"><b>${item.name} :</b>
                                 <label class="news-date" style="font-size: 13px">(<fmt:formatDate type = "both"
                                                                                                  dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${item.createdDate}"/>)
                                </label>
                                </span>
                                <ul style="list-style: none">
                                    <li>${item.comment}</li>
                                </ul>
                                <hr>
                            </c:forEach>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-sm-12 news-footer">
                <div class="header-news-footer">
                    <h2>Các tin liên quan</h2>
                </div>
                <div class="body-news-footer">
                    <ul>
                        <c:forEach items="${itemNew}" var="item" begin="0" end="5">
                            <li><a href="/bai-viet/chi-tiet/${item.id}">${item.title}</a><br>
                                <label class="news-date" style="font-size: 13px"><fmt:formatDate type = "both"
                                    dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${item.createdDate}" />
                                </label></li>
                            <hr>
                        </c:forEach>
                    </ul>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.col-lg-9 -->
    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->
<script>
    $('#btnShare').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        share(data);
    });
    function share(data) {
        $.ajax({
            url: '${urlShare}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlNew}?comment_success";
            },
            error: function () {
                window.location.href = "${urlNew}?false";
            }
        });
    }

</script>
</body>

</html>
