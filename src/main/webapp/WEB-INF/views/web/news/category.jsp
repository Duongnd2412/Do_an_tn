<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Tin tức</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
</head>

<body>

<!-- Navigation -->

<!-- Page Content -->
<div class="container">
    <div class="row" style="width: 1300px;">
        <div class="col-sm-3">
            <h1 class="my-4"></h1>
            <div class="list-group nav-link" style="background-color: #fffad5;">
                <a style="color: #fcb71e;"><b>CHUYÊN MỤC</b></a>
                <ul>
                    <c:forEach items="${group}" var="item">
                        <a href="/bai-viet/nhom/the-loai/${item.id}">${item.name}</a>
                        <hr>
                        <ul>
                            <c:forEach items="${item.categoryName}" var="item1" varStatus="loop">
                                <li>
                                    <a href="/bai-viet/the-loai/${item.ids[loop.index]}">
                                        <div class="clearfix">
                                                ${item1}
                                        </div>
                                    </a>
                                    <hr>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
            </div>

        </div>
        <!-- /.col-lg-3 -->

        <div class="col-sm-9" style="padding-top: 20px">
            <div style="width: 900px;">
                <c:forEach var="item" items="${groupId.listNew}">
                    <div class=" news-item row">
                        <div class="col-md-3">
                            <img src="${item.thumbnail}" alt="" style="width: 200px;height: 120px">
                        </div>
                        <div class="col-md-8">
                                <a href="/bai-viet/chi-tiet/${item.id}">
                                    <h6>${item.title}</h6>
                                </a>
                            <label class="limit-text">
                                    ${item.shortDescription}
                            </label>
                            <span ><fmt:formatDate type = "both"
                                                    dateStyle = "short" timeStyle = "short" pattern="dd-M-yyyy" value = "${item.createdDate}" />
                            </span>
                            <hr>
                        </div>
                    </div>
                    <br>
                </c:forEach>
            </div>

            <!-- /.row -->

        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->
</div>
<!-- /.container -->

<!-- Footer -->



</body>

</html>
