<%@ include file="/common/taglib.jsp"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Thể thao TV</title>

</head>

<body>
<div style="background-color: #f6f6f6">
    <div id="carouselExampleIndicators"  class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
        <c:forEach items="${banner}" var="item">
            <div class="carousel-item active">
                <img class="d-block img-fluid" src="${item.banner1}" >
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="${item.banner2}" >
            </div>
            <div class="carousel-item">
                <img class="d-block img-fluid" src="${item.banner3}" >
            </div>
        </c:forEach>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
    <div class="container" style="background-color: white">
        <div class="row ">
            <!--  /.col-lg-3 -->
            <div >
                <div class="row">
                    <div class="col-md-3 col-sm-12 ">
                        <div style="background-color: #f02904" class="mt-4">
                            <b style="font-size: 20px;color: white">Tin Hot</b>
                        </div>
                        <div class="card">
                            <ul style="list-style-type: circle; width: 100%;">
                                <c:forEach var="item" items="${newCount}" begin="0" end="9">
                                    <li><a href="/bai-viet/chi-tiet/${item.id}">${item.title}</a></li>
                                    <hr>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-12 row ">
                        <div class="col-md-8 col-sm-12">
                            <br>
                            <div class="card h-auto tong" style="border: none;">
                                <c:forEach var="item1" items="${group}" >
                                    <c:if test="${item1.code == 1}">
                                        <c:forEach var="item" items="${item1.listNew}" begin="0" end="0">
                                            <img class="card-img-top layer1" src="${item.thumbnail}" alt="">
                                            <div class="layer2"> </div>
                                            <a class="layer3" href="/bai-viet/chi-tiet/${item.id}"><b>${item.title}</b></a><br>
                                        </c:forEach>
                                   </c:if>
                               </c:forEach>
                            </div>
                            <div class="row">
                                <c:forEach var="item1" items="${group}">
                                    <c:if test="${item1.code == 1 || item1.code == 2 }">
                                        <c:forEach var="item" items="${item1.listNew}" begin="1" end="3">
                                            <div class="row" >
                                                <img class="col-md-3 col-sm-12" src="${item.thumbnail}" style="margin-right: 0">
                                                <div class="col-md-8 col-sm-12">
                                                    <a  href="/bai-viet/chi-tiet/${item.id}"><b>${item.title}</b></a><br>
                                                    <label style="font-size: 13px">
                                                        -<fmt:formatDate type = "both"
                                                                         dateStyle = "short" timeStyle = "short" pattern="dd/M/yyyy" value = "${item.createdDate}"/>-
                                                    </label>
                                                </div>
                                            </div>
                                            <br>
                                        </c:forEach>
                                    </c:if>
                                </c:forEach>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <br>
                            <div  class="row">
                                <div class="card h-100 tong" style="border: none;width: 100%">
                                <c:forEach var="item1" items="${group}">
                                    <c:if test="${item1.code == 2}">
                                        <c:forEach var="item" items="${item1.listNew}" begin="0" end="0">
                                            <img class="card-img-top layer11" src="${item.thumbnail}" alt="">
                                            <div class="layer22"> </div>
                                            <a class="layer33" href="/bai-viet/chi-tiet/${item.id}"><b>${item.title}</b></a><br>
                                        </c:forEach>
                                    </c:if>
                                </c:forEach>
                                </div>
                            </div>
                            <div  class="row">
                                <div class="card h-100 tong" style="border: none;">
                                    <c:forEach var="item1" items="${group}">
                                        <c:if test="${item1.code == 3}">
                                            <c:forEach var="item" items="${item1.listNew}" begin="0" end="0">
                                                <img class="card-img-top layer11" src="${item.thumbnail}" alt="">
                                                <div class="layer22"> </div>
                                                <a class="layer33" href="/bai-viet/chi-tiet/${item.id}"><b>${item.title}</b></a><br>
                                            </c:forEach>
                                        </c:if>
                                    </c:forEach>
                                </div>
                                <div>
                                    <c:forEach items="${group}" var="item1">
                                        <c:if test="${item1.code == 4}">
                                            <c:forEach var="item" items="${item1.listNew}" begin="0" end="6">
                                                <a class="col-md-8" href="/bai-viet/chi-tiet/${item.id}">${item.title}</a>
                                                <hr>
                                            </c:forEach>
                                        </c:if>
                                    </c:forEach>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.col-lg-9 -->
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12" >
                <c:forEach var="item" items="${category}">
                    <c:if test="${item.code == 1}">
                        <div class="head-top">
                            <a style="font-size: 20px; color: white" href="/bai-viet/the-loai/${item.id}">${item.name}
                            </a>
                        </div>
                        <c:forEach var="item1" items="${item.listNew}" begin="0" end="6">
                            <div class="row" style="margin-top: 8px">
                                <img class="col-md-3" src="${item1.thumbnail}" style="height: 75px">
                                <div class="col-md-8">
                                    <a  href="/bai-viet/chi-tiet/${item1.id}">${item1.title}</a><br>
                                    <label style="font-size: 13px">
                                        -<fmt:formatDate type = "both"
                                                         dateStyle = "short" timeStyle = "short" pattern="dd/M/yyyy" value = "${item1.createdDate}"/>-
                                    </label>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>
            </c:forEach>
            </div>
            <div class="col-md-6 col-sm-12" >
                <c:forEach var="item" items="${category}">
                    <c:if test="${item.code == 2}">
                        <div class="head-top">
                                <a style="font-size: 20px; color: white" href="/bai-viet/the-loai/${item.id}">${item.name}
                                </a>
                        </div>
                        <c:forEach var="item1" items="${item.listNew}" begin="0" end="6">
                            <div class="row" style="margin-top: 8px">
                                <img class="col-md-3" src="${item1.thumbnail}" style="height: 75px">
                                <div class="col-md-8">
                                    <a  href="/bai-viet/chi-tiet/${item1.id}">${item1.title}</a><br>
                                    <label style="font-size: 13px">
                                        -<fmt:formatDate type = "both"
                                                         dateStyle = "short" timeStyle = "short" pattern="dd/M/yyyy" value = "${item1.createdDate}"/>-
                                    </label>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <div class="row" style="margin-top: 10px">
            <div class="col-md-4 col-sm-12">
                <c:forEach var="item" items="${category}">
                    <c:if test="${item.code == 7}">
                        <div class="head-top">
                            <a style="font-size: 20px; color: white" href="/bai-viet/the-loai/${item.id}">${item.name}
                            </a>
                        </div>
                        <c:forEach var="item1" items="${item.listNew}" begin="0" end="2">
                            <div class="row">
                                <div class="container">
                                    <a  href="/bai-viet/chi-tiet/${item1.id}">${item1.title}</a><br>
                                    <label style="font-size: 13px">
                                        -<fmt:formatDate type = "both"
                                                         dateStyle = "short" timeStyle = "short" pattern="dd/M/yyyy" value = "${item1.createdDate}"/>-
                                    </label>
                                </div>
                            </div>
                            <hr>
                        </c:forEach>
                    </c:if>
                </c:forEach>
            </div>
            <div class="col-md-6 col-sm-12">
                <c:forEach var="item" items="${category}">
                    <c:if test="${item.code == 6}">
                        <div class="head-top">
                            <a style="font-size: 20px; color: white" href="/bai-viet/the-loai/${item.id}">${item.name}
                            </a>
                        </div>
                        <div class="row" style="margin-top: 8px">
                            <c:forEach var="item1" items="${item.listNew}" begin="0" end="0">
                                    <img class="col-md-6 col-sm-12" src="${item1.thumbnail}" style="height: 130px">
                                    <div class="col-md-6 col-sm-12">
                                        <a  href="/bai-viet/chi-tiet/${item1.id}"><b>${item1.title}</b></a><br>
                                        <label style="font-size: 13px">
                                            ${item1.shortDescription}
                                        </label>
                                    </div>
                            </c:forEach>
                        </div>
                        <div class="row" style="margin-top: 8px">
                            <div class="col-md-6 col-sm-12">
                                <c:forEach var="item1" items="${item.listNew}" begin="1" end="1">
                                    <img src="${item1.thumbnail}" style="height: 120px;width: 100%">
                                    <a  href="/bai-viet/chi-tiet/${item1.id}"><b>${item1.title}</b></a><br>
                                </c:forEach>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <c:forEach var="item1" items="${item.listNew}" begin="2" end="2">
                                    <img src="${item1.thumbnail}" style="height: 120px;width: 100%">
                                    <a  href="/bai-viet/chi-tiet/${item1.id}"><b>${item1.title}</b></a><br>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>
                </c:forEach>
            </div>
            <div class="col-md-2 col-sm-12">
                <c:forEach var="item" items="${category}">
                    <c:if test="${item.code == 8}">
                        <div class="head-top">
                            <a style="font-size: 20px; color: white" href="/bai-viet/the-loai/${item.id}">${item.name}
                            </a>
                        </div>
                        <c:forEach var="item1" items="${item.listNew}" begin="0" end="1">
                            <div class="row" style="margin-top: 8px">
                                <div class="container">
                                    <img src="${item1.thumbnail}" style="height: 90px;width: 100%">
                                    <a style="font-size: 14px"  href="/bai-viet/chi-tiet/${item1.id}"><b>${item1.title}</b></a><br>
                                </div>
                            </div>
                        </c:forEach>
                    </c:if>
                </c:forEach>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <c:forEach var="item" items="${news}" begin="3">
                    <div class="row">
                        <img class="col-md-4" src="${item.thumbnail}" style="height: 130px">
                        <div class="col-md-8">
                            <a href="/bai-viet/chi-tiet/${item.id}"><b>${item.title}</b></a><br>
                            <label style="font-size: 14px">
                                    ${item.shortDescription}
                            </label>
                            <label style="font-size: 13px" class="pull-right">
                                -<fmt:formatDate type = "both"
                                                 dateStyle = "short" timeStyle = "short" pattern="dd/M/yyyy" value = "${item.createdDate}"/>-
                            </label>
                        </div>
                    </div>
                    <hr>
                </c:forEach>
            </div>
        </div>
    </div>
</div>


<!-- Footer -->
<!-- Bootstrap core JavaScript -->


</body>

</html>
