<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/infor/edit-banner/1"/>
<c:url var="APIurl" value="/admin/api/infor/edit-banner"/>
<html>
<head>
    <title>Chỉnh sửa banner</title>

</head>
<body>
<link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">

<div id="wrapper" style="width: 100%">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa thông tin website</b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <form:form modelAttribute="banner" cssClass="form-horizontal container" id="formSubmit" method="post"  cssStyle="padding-left: 30px">
                            <h1> </h1>
                            <br>

                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Banner1</label>
                                <div class="col-sm-9">
                                    <div class="avatar" >
                                        <img id="avatar"
                                             src="${banner.banner1}"
                                             class="img-fluid" style="max-width: 150px; max-height: 120px;" />
                                    </div>
                                    <div class="file-field">
                                        <p>
                                            <strong id="Ithumbnail">Chọn ảnh</strong><br />
                                            <button
                                                    class="btn btn-primary btn-sm waves-effect waves-light"
                                                    type="button" value="Browse Image"
                                                    onclick="BrowseServer( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                        </p>
                                    </div>
                                    <form:hidden  path="banner1" id="image_src"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Tiêu đề Banner1</label>
                                <div class="col-sm-9">
                                    <form:input path="name1" id="name1" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Banner2</label>
                                <div class="col-sm-9">
                                    <div class="avatar">
                                        <img id="avatar2"
                                             src="${banner.banner2}"
                                             class="img-fluid" style="max-width: 150px; max-height: 120px;" />
                                    </div>
                                    <div class="file-field">
                                        <p>
                                            <strong id="Ithumbnail2">Chọn ảnh</strong><br />
                                            <button
                                                    class="btn btn-primary btn-sm waves-effect waves-light"
                                                    type="button" value="Browse Image"
                                                    onclick="BrowseServer2( 'Images:/', 'Ithumbnail2' );" >Browse Image</button>
                                        </p>
                                    </div>
                                    <form:hidden  path="banner2" id="image_src2"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Tiêu đề Banner2</label>
                                <div class="col-sm-9">
                                    <form:input path="name2" id="name2" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Banner3</label>
                                <div class="col-sm-9">
                                    <div class="avatar">
                                        <img id="avatar3"
                                             src="${banner.banner3}"
                                             class="img-fluid" style="max-width: 150px; max-height: 120px;" />
                                    </div>
                                    <div class="file-field">
                                        <p>
                                            <strong id="Ithumbnail3">Chọn ảnh</strong><br />
                                            <button
                                                    class="btn btn-primary btn-sm waves-effect waves-light"
                                                    type="button" value="Browse Image"
                                                    onclick="BrowseServer3( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                        </p>
                                    </div>
                                    <form:hidden  path="banner3" id="image_src3"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Tiêu đề Banner3</label>
                                <div class="col-sm-9">
                                    <form:input path="name3" id="name3" class="form-control"/>
                                </div>
                            </div>
                            <form:hidden path="id" id="bannerId"/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div style="padding-left: 30%">
                                        <button type="submit"  class="btn btn-white btn-warning btn-bold"  id="btnUpdateNew"/>Cập nhật
                                        <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >

    var editor = '';
    /*Avatar start*/
    function BrowseServer(startupPath, functionData) {
        var finder = new CKFinder();
        finder.basePath = '../';
        finder.startupPath = startupPath;
        finder.selectActionFunction = SetFileField;
        finder.selectActionData = functionData;
        finder.popup();
    }
    function SetFileField(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("avatar").src = fileUrl;
        $('#avatar').val(fileUrl);
        $('#image_src').val(fileUrl);
    }

    function BrowseServer2(startupPath, functionData) {
        var finder = new CKFinder();
        finder.basePath = '../';
        finder.startupPath = startupPath;
        finder.selectActionFunction = SetFileField2;
        finder.selectActionData = functionData;
        finder.popup();
    }
    function SetFileField2(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("avatar2").src = fileUrl;
        $('#avatar2').val(fileUrl);
        $('#image_src2').val(fileUrl);
    }

    function BrowseServer3(startupPath, functionData) {
        var finder = new CKFinder();
        finder.basePath = '../';
        finder.startupPath = startupPath;
        finder.selectActionFunction = SetFileField3;
        finder.selectActionData = functionData;
        finder.popup();
    }
    function SetFileField3(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("avatar3").src = fileUrl;
        $('#avatar3').val(fileUrl);
        $('#image_src3').val(fileUrl);
    }





    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        updateNew(data);
    });
    function updateNew(data) {
        console.log('${APIurl}');
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}?&message=update_success";
            },
            error: function () {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
