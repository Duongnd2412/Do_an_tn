<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<c:url var="urlList" value="/admin/new/list?page=1&limit=5"/>
<c:url var="urlSearch" value="/admin/new/search-new"/>
<c:url var="APIurl" value="/admin/api/new/delete"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Danh sách bài viết</title>

		<!-- Custom styles for this template-->
		<link href="<c:url value='/css/admin/sb-admin.css'/>" rel="stylesheet">


	</head>
	<body id="page-top">
		<div >
			<div class="main-content-inner">
				<ul class="breadcrumb">
					<li>
						<ol class="breadcrumb">
							<li class="breadcrumb-item">
								<a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
							</li>
							<li class="breadcrumb-item active"><b>Danh sách bài viết</b></li>
						</ol>
					</li>
				</ul>
			</div>

			<div class="container-fluid" style="width: 100%">
				<form  action="/admin/category/search-new" method="get">
					<div class="row">
						<div class="col-md-6 row">
							<select class="form-control col-md-8" id="id" name="id">
								<option>--Chọn thể loại--</option>
								<c:forEach var="item" items="${category}">
									<option value="${item.id}">${item.name}</option>
								</c:forEach>
							</select>
							<button class="col-md-4" type="submit">Tìm theo thể loại</button>
						</div>
					</div>
				</form>
				<form id="formSubmit">
					<div class="main-content-inner">

						<div class="page-content">
							<div class="row">
								<div class="col-md-12">
									<c:if test="${not empty message}">
										<div class="alert alert-${alert}">
												${message}
										</div>
									</c:if>
									<hr>
									<div class="widget-box table-filter row">
										<div class="table-btn-controls row" style="width: 100%">
											<div class="col-md-6 pull-left">

											</div>
											<div class="col-md-6">
												<div class="pull-right">
													<c:forEach items="${roles}" var="item">
														<c:if test="${item == 'EDIT_NEW'}">
															<a flag="info"
															   class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" data-toggle="tooltip"
															   title='Thêm bài viết' href="<c:url value='/admin/new/create-new'/>">
																<span>
																	<i class="fa fa-plus-circle bigger-110 purple"></i>
																</span>
															</a>
														</c:if>
														<c:if test="${item == 'DELETE'}">
															<button id="btnDelete" type="button" onclick="warningBeforeDelete()"
																	class="dt-button buttons-html5 btn btn-white btn-primary btn-bold" data-toggle="tooltip" title='Xóa'>
																<span>
																	<i class="fa fa-trash" aria-hidden="true"></i>
																</span>
															</button>
														</c:if>
													</c:forEach>
												</div>
											</div>
										</div>
										<hr>
									</div>
									<div class="row">
										<div class="col-xs-12">
											<div class="table-responsive">
												<table class="table table-bordered">
													<thead>
													<tr>
														<th></th>
														<th>Tên bài viết</th>
														<th>Mô tả ngắn</th>
														<th>Thể loại</th>
														<th>Lượt xem</th>
														<th>Người viết bài</th>
														<th>Ngày tạo</th>
														<th>Người sửa bài</th>
														<th>Ngày sửa</th>
														<th>Thao tác</th>
													</tr>
													</thead>
													<tbody>
													<c:forEach var="item" items="${newDto}">
														<tr>
															<td><input type="checkbox" id="checkbox_${item.id}" value="${item.id}"></td>
															<td>${item.title}</td>
															<td id="shortDescription">${item.shortDescription}</td>
															<td>${item.categoryName}</td>
															<td>${item.count}</td>
															<td>${item.createdBy}</td>
															<td>
																<fmt:formatDate type = "both"
																				dateStyle = "short" timeStyle = "short"
																				pattern="dd-M-yyyy" value = "${item.createdDate}" />
															</td>
															<td>${item.modifiedBy}</td>
															<td>
																<fmt:formatDate type = "both"
																				dateStyle = "short" timeStyle = "short"
																				pattern="dd-M-yyyy" value = "${item.modifiedDate}" />
															</td>
															<td>
																<div class="dt-buttons btn-overlap btn-group">
																	<c:forEach items="${roles}" var="itemRole">
																		<c:if test="${itemRole == 'EDIT_NEW'}">
																			<a flag="info"
																			   class="dt-button buttons-colvis btn btn-white btn-primary btn-bold" data-toggle="tooltip"
																					   title='Cập nhật bài viết' href='<c:url value='/admin/new/edit-new/${item.id}'/>'>
																				<span>
																					<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
																				</span>
																			</a>
																		</c:if>
																	</c:forEach>
																</div>
															</td>
														</tr>
													</c:forEach>
													</tbody>
												</table>
												<ul class="pagination" id="pagination"></ul>
												<input type="hidden" id="page" name="page">
												<input type="hidden" id="limit" name="limit">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>

<!-- /.main-content -->
<script >

			var totalPages = ${model.totalPage};
			var currenPage = ${model.page}
			$(document).ready(function () {
				window.pagObj = $('#pagination').twbsPagination({
					totalPages: totalPages,
					visiblePages: 5,
					startPage: currenPage,
					onPageClick: function (event, page) {
						if (currenPage != page){
							$('#page').val(page);
							$('#limit').val(5);
							$('#formSubmit').submit();
						}
					}
				});
			});
			<%--var string = '${item.shortDescription}';--%>
			<%--document.write(string.slice(0,50)+"...");--%>
			function warningBeforeDelete() {
				swal({
					title: "Xác nhận xóa",
					text: "Bạn có chắc chắn muốn xóa hay không",
					type: "warning",
					showCancelButton: true,
					confirmButtonClass: "btn-danger",
					confirmButtonText: "Xác nhận",
					cancelButtonText: "Hủy bỏ",
				}).then(function(isConfirm) {
					if (isConfirm.value) {
						var ids = $('tbody input[type=checkbox]:checked').map(function () {
							return $(this).val();
						}).get();
						console.log(ids);
						deleteNew(ids);
					}

				});
			}
			function deleteNew(data) {
				$.ajax({
					url: '${APIurl}',
					type: 'DELETE',
					contentType: 'application/json',
					data: JSON.stringify(data),
					success: function () {
						window.location.href = "${urlList}&message=delete_success";
					},
					error: function () {
						window.location.href = "${urlList}&message=error_system";
					}
				});
			}
</script>
	</body>
	</html>
