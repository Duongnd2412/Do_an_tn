<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/user/role"/>
<c:url var="APIurl" value="/admin/api/user/permission/${groups.id}"/>
<html>
<head>
    <title>Chỉnh sửa quyền chi tiết</title>

</head>
<body>
<link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">

<div id="wrapper" style="width: 100%">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa chi tiết quyền </b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <form class="form-horizontal container" id="formSubmit" action="/admin/api/user/permission/${groups.id}"  style="padding-left: 30px">
                            <h1 style="padding-left: 20%">${groups.name}</h1>
                            <br>
                            <c:forEach var="item" items="${permissions}">
                                <c:if test="${item.checked == 1}">
                                    <div style="padding-left: 30%">
                                        <div class="form-check " >
                                            <input class="form-check-input" checked="checked" name="permissionId" type="checkbox" value="${item.id}" id="permissionId${item.id}">
                                            <label class="form-check-label" >
                                                    ${item.name}
                                            </label>
                                        </div>
                                    </div>
                                </c:if>
                                <c:if test="${item.checked != 1}">
                                    <div style="padding-left: 30%">
                                        <div class="form-check" >
                                            <input class="form-check-input"  name="permissionId" type="checkbox" value="${item.id}" id="permissionId${item.id}">
                                            <label class="form-check-label" >
                                                    ${item.name}
                                            </label>
                                        </div>
                                    </div>
                                </c:if>
                            </c:forEach>
                            <hidden name="id" id="roleId"/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div style="padding-left: 30%">
                                    <button type="submit"  class="btn btn-white btn-warning btn-bold"  id="btnUpdateNew"/>Cập nhật
                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        var dataList = [];
        $.each(formData, function (i, v) {
            if (v.name == 'permissionId' ) {
                dataList.push(v.value);
            }
        });
        data['permissionId'] = dataList;
        updateNew(data);
    });
    function updateNew(data) {
        console.log('${APIurl}');
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}?&message=update_success";
            },
            error: function () {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
