<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%--<c:url var="newURL" value='/admin/new/list'/>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Danh sách quyền hạn</title>
</head>
<body>

<div id="wrapper" style="width: 100%">

    <div class="container-fluid" style="width: 100%">
        <form action="<c:url value='/'/>" id="formSubmit" method="get">
            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                        </li>
                        <li class="breadcrumb-item active"><b>Danh sách Quyền</b></li>
                    </ul>
                    <!-- /.breadcrumb -->
                </div>
                <div class="page-content container">
                    <div class="row" >
                        <div class="col-xs-12 container">
                            <c:if test="${not empty message}">
                                <div class="alert alert-${alert}">
                                        ${message}
                                </div>
                            </c:if>
                            <hr>
                            <div class="widget-box table-filter row" >
                                <div class="table-btn-controls" style="width: 100%">
                                    <div class="pull-right tableTools-container">
                                        <div class="dt-buttons btn-overlap btn-group" >
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Tên quyền</th>
                                                <th>Thao tác</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="item" items="${role}">
                                                <tr>
                                                    <td>${item.name}</td>
                                                    <td>
                                                        <c:if test="${item.id != 1}">
                                                            <div style="padding-left: 40%">
                                                                <a  class=" btn btn-sm btn-primary btn-edit" data-toggle="tooltip"
                                                                   title="Chỉnh sửa chi tiết" href="/admin/user/role/${item.id}"><i class="container fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                </a>
                                                            </div>
                                                        </c:if>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.main-content -->
<script >
</script>
</body>
</html>
