<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/infor/edit/1"/>
<c:url var="APIurl" value="/admin/api/infor/edit"/>
<html>
<head>
    <title>Chỉnh sửa thông tin website</title>

</head>
<body>
<link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">

<div id="wrapper" style="width: 100%">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa thông tin website</b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <form:form modelAttribute="infors" cssClass="form-horizontal container" id="formSubmit" method="post"  cssStyle="padding-left: 30px">
                            <h1> </h1>
                                <br>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Logo website</label>
                                <div class="col-sm-9">
                                    <div class="avatar" >
                                        <img id="avatar"
                                             src="${infors.logo}"
                                             class="img-fluid" style="max-width: 150px; max-height: 120px;" />
                                    </div>
                                    <div class="file-field">
                                        <p>
                                            <strong id="Ithumbnail">Chọn ảnh</strong><br />
                                            <button
                                                    class="btn btn-primary btn-sm waves-effect waves-light"
                                                    type="button" value="Browse Image"
                                                    onclick="BrowseServer( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                        </p>
                                    </div>
                                    <form:hidden  path="logo" id="image_src"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Tiêu đề footer</label>
                                <div class="col-sm-9">
                                    <form:input path="name" id="name" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Fan Page</label>
                                <div class="col-sm-9">
                                    <form:input path="groupName" id="groupName" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Địa chỉ website</label>
                                <div class="col-sm-9">
                                    <form:input path="websiteName" id="websiteName" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 control-label no-padding-right">Email</label>
                                <div class="col-sm-9">
                                    <form:input path="email" id="email" class="form-control"/>
                                </div>
                            </div>
                            <form:hidden path="id" id="inforId"/>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div style="padding-left: 30%">
                                        <button type="submit"  class="btn btn-white btn-warning btn-bold"  id="btnUpdateNew"/>Cập nhật
                                        <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                    </div>
                                </div>
                            </div>

                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >

    var editor = '';
    /*Avatar start*/
    function BrowseServer(startupPath, functionData) {
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();

        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.basePath = '../';

        //Startup path in a form: "Type:/path/to/directory/"
        finder.startupPath = startupPath;

        // Name of a function which is called when a file is selected in CKFinder.
        finder.selectActionFunction = SetFileField;

        // Additional data to be passed to the selectActionFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        finder.selectActionData = functionData;

        // Name of a function which is called when a thumbnail is selected in CKFinder. Preview img
        // finder.selectThumbnailActionFunction = ShowThumbnails;

        // Launch CKFinder
        finder.popup();
    }

    function SetFileField(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("avatar").src = fileUrl;
        $('#avatar').val(fileUrl);
        $('#image_src').val(fileUrl);
    }

    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        updateNew(data);
    });
    function updateNew(data) {
        console.log('${APIurl}');
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}?&message=update_success";
            },
            error: function () {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
