<%@include file="/common/taglib.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<c:url var="urlList" value="/admin/feedback/list?page=1&limit=10"/>
<c:url var="APIurl" value="/admin/api/feedback/delete"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Feedback</title>
</head>
<body>


<div id="wrapper" style="width: 100%">


    <div class="container-fluid" style="width: 100%">
        <form action="<c:url value='/admin/feedback/list'/>" id="formSubmit" method="get">

            <div class="main-content-inner">
                <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                        </li>
                        <li class="breadcrumb-item active"><b>Feedback</b></li>
                    </ul>
                    <!-- /.breadcrumb -->
                </div>
                <div class="page-content container">
                    <div class="row" style="padding-left: 70px">
                        <div class="col-xs-12">
                            <c:if test="${not empty message}">
                                <div class="alert alert-${alert}">
                                        ${message}
                                </div>
                            </c:if>
                            <hr>
                            <div class="widget-box table-filter row">
                                <div class="table-btn-controls" style="width: 100%">
                                    <div class="pull-right tableTools-container">
                                        <div class="dt-buttons btn-overlap btn-group">
                                            <button id="btnDelete" type="button" onclick="warningBeforeDelete()"
                                                    class="dt-button buttons-html5 btn btn-white btn-primary btn-bold" data-toggle="tooltip" title='Xóa'>
																<span>
																	<i class="fa fa-trash" aria-hidden="true"></i>
																</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Email</th>
                                                <th>Nội dung</th>
                                                <th>Ngày gửi</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <c:forEach var="item" items="${feedback}">
                                                <tr>
                                                    <td><input type="checkbox" id="checkbox_${item.id}" value="${item.id}"></td>
                                                    <td>${item.email}</td>
                                                    <td>${item.content}</td>
                                                    <td>
                                                        <fmt:formatDate type = "both"
                                                                        dateStyle = "short" timeStyle = "short"
                                                                        pattern="dd-M-yyyy" value = "${item.createdDate}" />
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <ul class="pagination" id="pagination"></ul>
                                        <input type="hidden" id="page" name="page">
                                        <input type="hidden" id="limit" name="limit">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.main-content -->
<script >
    var totalPages = ${model.totalPage};
    var currenPage = ${model.page}
        $(function () {
            window.pagObj = $('#pagination').twbsPagination({
                totalPages: totalPages,
                visiblePages: 10,
                startPage: currenPage,
                onPageClick: function (event, page) {
                    if (currenPage != page){
                        $('#page').val(page);
                        $('#limit').val(10);
                        $('#formSubmit').submit();
                    }
                }
            });
        });
    function warningBeforeDelete() {
        swal({
            title: "Xác nhận xóa",
            text: "Bạn có chắc chắn muốn xóa hay không",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Xác nhận",
            cancelButtonText: "Hủy bỏ",
        }).then(function(isConfirm) {
            if (isConfirm.value) {
                var ids = $('tbody input[type=checkbox]:checked').map(function () {
                    return $(this).val();
                }).get();
                console.log(ids);
                deleteNew(ids);
            }

        });
    }
    function deleteNew(data) {
        $.ajax({
            url: '${APIurl}',
            type: 'DELETE',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function () {
                window.location.href = "${urlList}&message=delete_success";
            },
            error: function () {
                window.location.href = "${urlList}&message=error_system";
            }
        });
    }
</script>
</body>
</html>
