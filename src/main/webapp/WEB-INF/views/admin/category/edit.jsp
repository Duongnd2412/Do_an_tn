<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:url var="urlList" value="/admin/category/list?page=1&limit=10"/>
<c:url var="APIurl" value="/admin/api/category/edit"/>
<html>
<head>
    <title>Chỉnh sửa bài viết</title>


</head>
<body>
<link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">

<div id="wrapper" style="width: 100%">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner" >
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                            </li>
                            <li class="breadcrumb-item active"><b>Chỉnh sửa thể loại</b></li>
                        </ol>
                    </li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <form:form id="formSubmit" role="form" modelAttribute="categories" cssClass="form-horizontal container" >
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Tên thể loại</label>
                                <div class="col-sm-9">
                                    <form:textarea path="name" id="name" style="width: 600px;height: 55px"/>
                                </div>
                            </div>
                            <div class="form-group" style="padding-left: 10px">
                                <label class="col-sm-3 control-label no-padding-right">Nhóm thể loại</label>
                                <div class="col-sm-9">
                                    <c:forEach var="item" items="${groups}">
                                        <form:radiobutton path="groupId" value="${item.id}" label="${item.name}"/>
                                        <br>
                                    </c:forEach>
                                </div>
                            </div>
                            <form:hidden path="id" id="categoryId"/>
                            <div class="form-group">
                                <div class="col-sm-5">
                                    <button type="submit"  class="btn btn-white btn-warning btn-bold" value="Cập nhật bài viết" id="btnAddOrUpdateNew"/>Cập nhật
                                    <i class="ace-icon fa fa-arrow-right icon-on-right bigger-110"></i></button>
                                </div>
                            </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script >
    // var editor = '';
    // $(document).ready(function(){
    // 	editor = CKEDITOR.replace( 'content');
    // });

    $('#btnAddOrUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        // data["content"] = editor.getData();
        var id = $('#id').val();
        updateNew(data);
    });
    function updateNew(data) {
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (result) {
                window.location.href = "${urlList}&message=update_success";
            },
            error: function (error) {
                window.location.href = "${urlList}?message=error_system";
            }
        });
    }
</script>
</body>
</html>
