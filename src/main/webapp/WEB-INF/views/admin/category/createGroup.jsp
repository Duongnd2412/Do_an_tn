<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%--<c:url var="APIurl" value="/admin/new/create">--%>
<c:url var="urlList" value="/admin/group-category/list?page=1&limit=10"/>
<c:url var="APIurl" value="/admin/api/category/create-group"/>
<html>
<head>
    <title>Thêm thể loại</title>
</head>
<body>
<link href="<c:url value='/css/admin/user.css'/>" rel="stylesheet">


<div id="wrapper" style="width: 100%">

    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>
                <ul class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="<c:url value="/admin"/>"><i class="ace-icon fa fa-home home-icon"></i><b>Trang chủ</b></a>
                    </li>
                    <li class="breadcrumb-item active"><b>Thêm nhóm thể loại</b></li>
                </ul><!-- /.breadcrumb -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>

                        <form class="container" id="formSubmit" action="/admin/api/category/create-group" method="post">
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Tên nhóm thể loại</label>
                                <div class="col-sm-9">
                                    <textarea rows="" cols="" id="name" name="name" style="width: 600px;height: 55px"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit"  class="btn btn-white btn-warning btn-bold" value="Thêm" id="btnUpdateNew">Thêm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#btnUpdateNew').click(function (e) {
        e.preventDefault();
        var data = {};
        var formData = $('#formSubmit').serializeArray();
        $.each(formData, function (i, v) {
            data[""+v.name+""] = v.value;
        });
        updateNew(data);
    });
    function updateNew(data) {
        $.ajax({
            url: '${APIurl}',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            dataType: 'json',
            success: function () {
                window.location.href = "${urlList}&message=insert_success";
            },
            error: function () {
                window.location.href = "${urlList}&message=error_system";
            }
        });
    }
</script>
</body>
</html>

