package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.NewDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface NewService  {
    NewDto findById(Long id);
    NewDto update(NewDto newDto);
    NewDto create(NewDto newDto);
    void delete(long[] ids);
    List<NewDto> findAll(Pageable pageable);
    int getTotalItem();
    List<NewDto> findAll(Pageable pageable,NewDto newDto);
    List<NewDto> findAll(NewDto newDto);
    List<NewDto> findAll(Sort sort);
    void countViews(Long id);


}
