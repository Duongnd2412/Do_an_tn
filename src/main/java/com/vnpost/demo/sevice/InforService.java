package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.InforDto;

import java.util.List;

public interface InforService {
    InforDto update(InforDto inforDto);
    InforDto findById(long id);
    List<InforDto> findAll();
}
