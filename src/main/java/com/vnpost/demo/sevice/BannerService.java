package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.BannerDto;

import java.util.List;

public interface BannerService {
    BannerDto update(BannerDto bannerDto);
    BannerDto findById(long id);
    List<BannerDto> findAll();
}
