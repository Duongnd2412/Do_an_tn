package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.CommentDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CommentService {
    List<CommentDto> findAll(Pageable pageable);
    CommentDto create(CommentDto commentDto);
    int getTotalItem();
    void delete(long[] id);
}
