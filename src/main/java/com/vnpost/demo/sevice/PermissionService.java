package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.PermissionDto;

import java.util.List;

public interface PermissionService {
    List<PermissionDto> findAll();
    PermissionDto findByUserName(String userName);
}
