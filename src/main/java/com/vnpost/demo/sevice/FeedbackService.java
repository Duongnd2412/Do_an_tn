package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.dto.FeedbackDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface FeedbackService {
    FeedbackDto create(FeedbackDto feedbackDto);
    int getTotalItem();
    List<FeedbackDto> findAll(Pageable pageable);
    void delete(long[] id);
}
