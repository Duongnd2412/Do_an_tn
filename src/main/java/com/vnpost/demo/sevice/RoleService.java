package com.vnpost.demo.sevice;

import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.dto.UserDto;

import java.util.List;

public interface RoleService  {
    List<RoleDto> findAll();
    RoleDto updatePermissionGr(RoleDto roleDto);
    RoleDto findById(Long id);

}
