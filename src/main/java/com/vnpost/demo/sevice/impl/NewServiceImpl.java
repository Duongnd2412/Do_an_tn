package com.vnpost.demo.sevice.impl;
import com.vnpost.demo.converter.CommentConverter;
import com.vnpost.demo.converter.NewConverter;
import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.repository.CategoryRepository;
import com.vnpost.demo.repository.NewRepository;
import com.vnpost.demo.repository.specification.NewSpecification;
import com.vnpost.demo.sevice.NewService;
import com.vnpost.demo.util.CopyUtil;
import com.vnpost.demo.util.SecurityUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class NewServiceImpl implements NewService {

    @Autowired
    private NewRepository newRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private NewConverter converter;
    @Autowired
    private CommentConverter commentConverter;


    @Override
    public NewDto findById(Long id) {
        Optional<NewEntity> newEntity = newRepository.findById(id);
        List<CommentDto> commentDtos = newEntity.get().getCommentId().stream().map(item ->commentConverter.toDto(item)).collect(Collectors.toList());
        NewDto newDto = converter.toDto(newEntity);
        newDto.setListComment(commentDtos);
        return newDto;
    }

    @Override
    public NewDto update(NewDto newDto) {
        NewEntity odlNew = newRepository.findById(newDto.getId()).get();
        NewEntity newNews = modelMapper.map(newDto,NewEntity.class);
        CopyUtil.copyOldToNewModel(odlNew,newNews);
        newNews.setCreatedBy(odlNew.getCreatedBy());
        newNews.setCreatedDate(odlNew.getCreatedDate());
        newNews.setModifiedDate(new Date());
        newNews.setModifiedBy(SecurityUtils.getPrincipal().getUsername());
        CategoryEntity categoryEntity =categoryRepository.findOneByName(newDto.getCategoryName());
        newNews.setCategoryId(categoryEntity);
        newRepository.save(newNews);
        return newDto;
    }

    @Override
    public NewDto create(NewDto newDto) {
        NewEntity newEntity = modelMapper.map(newDto, NewEntity.class);
        CategoryEntity categoryEntity =categoryRepository.findOneByName(newDto.getCategoryName());
        newEntity.setCategoryId(categoryEntity);
        newRepository.save(newEntity);
        return newDto;
    }

    @Override
    public void delete(long[] ids) {
        for (Long id: ids) {
            newRepository.deleteById(id);
        }
    }
    @Override
    public void countViews(Long id) {
        NewEntity news = newRepository.findOneById(id);
        if (news.getCount()==null){
            news.setCount(1);
        }else {
            news.setCount(news.getCount()+1);
        }
        newRepository.save(news);
    }



    @Override
    public List<NewDto> findAll(Pageable pageable) {
        List<NewEntity> newEntities = newRepository.findAll(pageable).getContent();
        return newEntities
                .stream()
                .map(newEntity -> converter.toDto1(newEntity))
                .collect(Collectors.toList());
    }

    @Override
    public int getTotalItem() {
        return (int) newRepository.count();
    }


    @Override
    public List<NewDto> findAll(Pageable pageable,NewDto newDto) {
        List<NewEntity> newEntities = (List<NewEntity>) newRepository.findAll(NewSpecification.filter(newDto),pageable);
        return newEntities
                .stream()
                .map(newEntity -> converter.toDto1(newEntity))
                .collect(Collectors.toList());
    }

    @Override
    public List<NewDto> findAll(NewDto newDto) {
        List<NewEntity> newEntities =newRepository.findAll(NewSpecification.filter(newDto));
        return newEntities
                .stream()
                .map(newEntity -> converter.toDto1(newEntity))
                .collect(Collectors.toList());
    }

    @Override
    public List<NewDto> findAll(Sort sort) {
        List<NewEntity> newEntities = newRepository.findAll(sort);
        return newEntities
                .stream()
                .map(newEntity -> converter.toDto1(newEntity))
                .collect(Collectors.toList());
    }





}
