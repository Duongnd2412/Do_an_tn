package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.dto.FeedbackDto;
import com.vnpost.demo.entity.CommentEntity;
import com.vnpost.demo.entity.FeedbackEntity;
import com.vnpost.demo.repository.FeedbackRepository;
import com.vnpost.demo.sevice.FeedbackService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private FeedbackRepository repository;
    @Autowired
    private JavaMailSender javaMailSender;

    @Override
    public FeedbackDto create(FeedbackDto feedbackDto) {
        FeedbackEntity feedbackEntity = modelMapper.map(feedbackDto,FeedbackEntity.class);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(feedbackDto.getEmail());
        message.setSubject("Thanks for your feedback");
        message.setText("Cảm ơn bạn đã góp ý cho website của chúng tôi !");
        javaMailSender.send(message);
        repository.save(feedbackEntity);
        return feedbackDto;
    }
    @Override
    public int getTotalItem() {
        return (int) repository.count();
    }

    @Override
    public List<FeedbackDto> findAll(Pageable pageable) {
        List<FeedbackEntity> feedbackEntities = repository.findAll(pageable).getContent();
        return feedbackEntities.stream()
                .map(item -> modelMapper.map(item, FeedbackDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(long[] id) {
        for (long ids : id){
            repository.deleteById(ids);
        }
    }
}
