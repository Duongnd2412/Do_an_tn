package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.InforDto;
import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.entity.InforEntity;
import com.vnpost.demo.entity.RoleEntity;
import com.vnpost.demo.entity.UserEntity;
import com.vnpost.demo.repository.InforRepository;
import com.vnpost.demo.sevice.InforService;
import com.vnpost.demo.util.CopyUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class InforServiceImpl implements InforService {
    @Autowired
    private InforRepository inforRepository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public InforDto update(InforDto inforDto) {
        long id = 1;
        InforEntity oldInFor = inforRepository.findById(inforDto.getId()).get();
        InforEntity newInFor = modelMapper.map(inforDto,InforEntity.class);
        CopyUtil.copyOldToNewModel(oldInFor,newInFor);
        inforRepository.save(newInFor);
        return inforDto;
    }

    @Override
    public InforDto findById(long id) {
        Optional<InforEntity> inforEntity = inforRepository.findById(id);
        return inforEntity
                .map(position ->modelMapper.map(position, InforDto.class))
                .orElse(null);
    }

    @Override
    public List<InforDto> findAll() {
        List<InforEntity>inforEntities = inforRepository.findAll();
        return inforEntities.stream()
                .map(roles -> modelMapper.map(roles, InforDto.class))
                .collect(Collectors.toList());
    }
}
