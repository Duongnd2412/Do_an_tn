package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.constant.Constant;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.entity.RoleEntity;
import com.vnpost.demo.entity.UserEntity;
import com.vnpost.demo.repository.RoleRepository;
import com.vnpost.demo.repository.UserRepository;
import com.vnpost.demo.sevice.UserService;
import com.vnpost.demo.util.CopyUtil;
import com.vnpost.demo.util.RandomPassword;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;


    @Override
    @Transactional
    public UserDto update(UserDto userDto) {
        UserEntity oldUser = userRepository.findById(userDto.getId()).get();
        UserEntity newUser = modelMapper.map(userDto,UserEntity.class);
        CopyUtil.copyOldToNewModel(oldUser,newUser);
        newUser.setCreatedBy(oldUser.getCreatedBy());
        newUser.setCreatedDate(oldUser.getCreatedDate());
        RoleEntity roleEntity = roleRepository.findOneById(userDto.getRoleId());
        newUser.setRoleId(roleEntity);
        userRepository.save(newUser);
        return userDto;
    }

    @Override
    public void delete(long[] id) {
        for (long ids : id){
            userRepository.deleteById(ids);
        }

    }

    @Override
    @Transactional
    public UserDto create(UserDto userDto) {
        UserEntity userEntity =modelMapper.map(userDto,UserEntity.class);
        userEntity.setPassword(passwordEncoder.encode(Constant.defaultPassword));
        RoleEntity roleEntity = roleRepository.findOneById(userDto.getRoleId());
        userEntity.setRoleId(roleEntity);
        userRepository.save(userEntity);
        return userDto;
    }


    @Override
    public List<UserDto> findAll() {
        List<UserEntity>userEntities = userRepository.findAll();
        return userEntities.stream()
                .map(users -> modelMapper.map(users, UserDto.class))
                .collect(Collectors.toList());
    }
    @Override
    public UserDto findById(Long id) {
        Optional<UserEntity> userEntity = userRepository.findById(id);
        return userEntity
                .map(position ->modelMapper.map(position, UserDto.class))
                .orElse(null);
    }
    @Override
    public void forgotPW(String email) {
        UserEntity userEntity = userRepository.findOneByEmail(email);
        String pw = RandomPassword.pwGenerate();
        if (userEntity != null) {
            userEntity.setPassword(passwordEncoder.encode(pw));
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(email);
            message.setSubject(" Password sender! \tfor username: " + userEntity.getEmail());
            message.setText("Password: " + pw);
            javaMailSender.send(message);
            userRepository.save(userEntity);
        }
    }

    @Override
    public UserDto findByEmail(String email) {
        UserEntity userEntity = userRepository.findOneByEmail(email);
        UserDto userDto = modelMapper.map(userEntity,UserDto.class);
        return userDto;
    }


}
