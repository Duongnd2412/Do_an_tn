package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.BannerDto;
import com.vnpost.demo.entity.BannerEntity;
import com.vnpost.demo.repository.BannerRepository;
import com.vnpost.demo.sevice.BannerService;
import com.vnpost.demo.util.CopyUtil;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerRepository repository;
    @Autowired
    private ModelMapper modelMapper;

    @Override
    public BannerDto update(BannerDto bannerDto) {
        BannerEntity oldBanner = repository.findById(bannerDto.getId()).get();
        BannerEntity newBanner = modelMapper.map(bannerDto,BannerEntity.class);
        CopyUtil.copyOldToNewModel(oldBanner,newBanner);
        repository.save(newBanner);
        return bannerDto;
    }

    @Override
    public BannerDto findById(long id) {
        Optional<BannerEntity> bannerEntity = repository.findById(id);
        return bannerEntity.map(item -> modelMapper.map(item,BannerDto.class)).orElse(null);
    }

    @Override
    public List<BannerDto> findAll() {
        List<BannerEntity> bannerEntity = repository.findAll();
        return bannerEntity.stream().map(item -> modelMapper.map(item,BannerDto.class)).collect(Collectors.toList());
    }
}
