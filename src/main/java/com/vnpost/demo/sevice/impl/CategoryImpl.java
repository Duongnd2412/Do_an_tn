package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.converter.CategoryConverter;
import com.vnpost.demo.converter.NewConverter;
import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.repository.CategoryRepository;
import com.vnpost.demo.repository.NewRepository;
import com.vnpost.demo.sevice.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CategoryImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private CategoryConverter categoryConverter;
    @Autowired
    private NewConverter newConverter;


    @Override
    public CategoryDto create(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryConverter.toEntity(categoryDto);
        categoryRepository.save(categoryEntity);
        return categoryDto;
    }

    @Override
    public void delete(long[] ids) {
        for (Long id : ids){
            categoryRepository.deleteById(id); 
        }
    }

    @Override
    public int getTotalItem() {
        return (int) categoryRepository.count();
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryRepository.findById(categoryDto.getId()).get();
        categoryEntity.setName(categoryDto.getName());
        categoryRepository.save(categoryEntity);
        return categoryDto;
    }

    @Override
    public List<CategoryDto> findAllNew() {
        List<CategoryEntity> categoryEntity = categoryRepository.findAll();
        List<CategoryDto> listCategory = new ArrayList<>();
        for (CategoryEntity categoryEntity1 : categoryEntity){
            List<NewDto> newDtos = new ArrayList<>();
            CategoryDto categoryDto = new CategoryDto();
            List<NewDto> newDtoList = categoryEntity1.getNewEntities().stream().map(item -> newConverter.toDto1(item)).collect(Collectors.toList());
            newDtos.addAll(newDtoList);
            Collections.sort(newDtos,(o1, o2) ->o2.getCreatedDate().compareTo(o1.getCreatedDate()));
            categoryDto.setListNew(newDtos);
            categoryDto.setId(categoryEntity1.getId());
            categoryDto.setName(categoryEntity1.getName());
            categoryDto.setCode(categoryEntity1.getCode());
            listCategory.add(categoryDto);
        }
        return listCategory;
    }

    @Override
    public List<CategoryDto> findAll(Pageable pageable) {
        List<CategoryEntity> categoryEntity = categoryRepository.findAll(pageable).getContent();
        return categoryEntity.stream()
                .map(category -> categoryConverter.toDto(category))
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDto findById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        return categoryEntity
                .map(item ->categoryConverter.toDto(item))
                .orElse(null);
    }

    @Override
    public CategoryDto findAllNewById(Long id) {
        Optional<CategoryEntity> categoryEntity = categoryRepository.findById(id);
        List<NewDto> newDtoList = categoryEntity.get().getNewEntities()
                                    .stream().map(item -> newConverter.toDto1(item)).collect(Collectors.toList());
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setListNew(newDtoList);
        return categoryDto ;
    }

    @Override
    public List<CategoryDto> findAll() {
        List<CategoryEntity> categoryEntity = categoryRepository.findAll();
        return categoryEntity.stream()
                .map(category -> categoryConverter.toDto(category))
                .collect(Collectors.toList());
    }
}
