package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.entity.CategoryEntity;
import com.vnpost.demo.entity.CommentEntity;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.repository.CommentRepository;
import com.vnpost.demo.repository.NewRepository;
import com.vnpost.demo.sevice.CommentService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private NewRepository newRepository;

    @Override
    public List<CommentDto> findAll(Pageable pageable) {
        List<CommentEntity> commentEntities = commentRepository.findAll(pageable).getContent();
        return commentEntities.stream()
                .map(item -> modelMapper.map(item,CommentDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public CommentDto create(CommentDto commentDto) {
        if (commentDto.getName() == ""){
            commentDto.setName("Anonymous");
        }
        CommentEntity commentEntity = modelMapper.map(commentDto, CommentEntity.class);
        NewEntity newEntity =newRepository.findOneById(commentDto.getNewId());
        commentEntity.setNewEntity(newEntity);
        commentRepository.save(commentEntity);
        return commentDto;
    }

    @Override
    public int getTotalItem() {
        return (int) commentRepository.count();
    }

    @Override
    public void delete(long[] id) {
        for (Long ids : id){
            commentRepository.deleteById(ids);
        }
    }
}
