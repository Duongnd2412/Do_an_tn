package com.vnpost.demo.sevice.impl;

import com.vnpost.demo.dto.PermissionDto;
import com.vnpost.demo.entity.PermissionEntity;
import com.vnpost.demo.repository.PermissionRepository;
import com.vnpost.demo.sevice.PermissionService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionImpl implements PermissionService {
    @Autowired
    private PermissionRepository permission;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<PermissionDto> findAll() {
        List<PermissionEntity> permissionEntities = permission.findAll();
        return permissionEntities
                .stream()
                .map(permissions -> modelMapper.map(permissions,PermissionDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public PermissionDto findByUserName(String userName) {

        return null;
    }
}
