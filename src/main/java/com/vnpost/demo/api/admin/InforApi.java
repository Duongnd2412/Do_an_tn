package com.vnpost.demo.api.admin;

import com.vnpost.demo.dto.BannerDto;
import com.vnpost.demo.dto.InforDto;
import com.vnpost.demo.sevice.BannerService;
import com.vnpost.demo.sevice.InforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/api/infor")
public class InforApi {
    @Autowired
    private InforService inforService;
    @Autowired
    private BannerService bannerService;

    @PutMapping("/edit")
    public InforDto update(@RequestBody InforDto inforDto){
        return inforService.update(inforDto);
    }

    @PutMapping("/edit-banner")
    public BannerDto updateBanner(@RequestBody BannerDto bannerDto){
        return bannerService.update(bannerDto);
    }
}
