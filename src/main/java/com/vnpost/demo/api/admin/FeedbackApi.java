package com.vnpost.demo.api.admin;

import com.vnpost.demo.sevice.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/api/feedback")
public class FeedbackApi {
    @Autowired
    private FeedbackService service;

    @DeleteMapping("/delete")
    public void delete(@RequestBody long[] id){
        service.delete(id);
    }
}
