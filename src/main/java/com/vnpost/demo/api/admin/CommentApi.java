package com.vnpost.demo.api.admin;

import com.vnpost.demo.sevice.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/api/comment")
public class CommentApi {
    @Autowired
    private CommentService commentService;


    @DeleteMapping("/delete")
    public void delete(@RequestBody long[] id){
        commentService.delete(id);
    }
}
