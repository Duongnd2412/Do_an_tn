package com.vnpost.demo.api.web;

import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.sevice.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/comment")
public class CommentWebApi {
    @Autowired
    private CommentService commentService;

    @PutMapping("/create")
    public CommentDto create(@RequestBody CommentDto commentDto){
        return commentService.create(commentDto);
    }
}
