package com.vnpost.demo.api.web;

import com.vnpost.demo.dto.FeedbackDto;
import com.vnpost.demo.sevice.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/feedback/api")
public class FeedbackWebApi {
    @Autowired
    private FeedbackService feedbackService;

    @PostMapping("/create")
    public FeedbackDto create(@RequestBody FeedbackDto feedbackDto){
        return feedbackService.create(feedbackDto);
    }
}
