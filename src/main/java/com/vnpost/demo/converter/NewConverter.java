package com.vnpost.demo.converter;

import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.NewEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class NewConverter {

    public NewDto toDto(Optional<NewEntity> newEntity){
        NewDto newDto = new NewDto();
        newDto.setId(newEntity.get().getId());
        newDto.setCategoryName(newEntity.get().getCategoryId().getName());
        newDto.setThumbnail(newEntity.get().getThumbnail());
        newDto.setShortDescription(newEntity.get().getShortDescription());
        newDto.setContent(newEntity.get().getContent());
        newDto.setTitle(newEntity.get().getTitle());
        newDto.setCount(newEntity.get().getCount());
        newDto.setCategoryId(newEntity.get().getCategoryId().getId());
        newDto.setCommentId(newEntity.get().getCommentId().stream().map(commentEntity -> commentEntity.getId()).collect(Collectors.toList()));
        newDto.setCodeGroupCategory(newEntity.get().getCategoryId().getGroupCategory().getCode());
        return newDto;
    }
    public NewDto toDto1(NewEntity newEntity){
        NewDto newDto = new NewDto();
        newDto.setId(newEntity.getId());
        newDto.setCategoryName(newEntity.getCategoryId().getName());
        newDto.setThumbnail(newEntity.getThumbnail());
        newDto.setShortDescription(newEntity.getShortDescription());
        newDto.setContent(newEntity.getContent());
        newDto.setTitle(newEntity.getTitle());
        newDto.setCount(newEntity.getCount());
        newDto.setCategoryId(newEntity.getCategoryId().getId());
        newDto.setCommentId(newEntity.getCommentId().stream().map(commentEntity -> commentEntity.getId()).collect(Collectors.toList()));
        newDto.setCreatedDate(newEntity.getCreatedDate());
        newDto.setCreatedBy(newEntity.getCreatedBy());
        newDto.setModifiedBy(newEntity.getModifiedBy());
        newDto.setModifiedDate(newEntity.getModifiedDate());
        newDto.setCodeGroupCategory(newEntity.getCategoryId().getGroupCategory().getCode());
        return newDto;
    }
    public NewEntity toEntity(NewDto newDto){
        NewEntity newEntity = new NewEntity();
        newEntity.setTitle(newDto.getTitle());
        newEntity.setShortDescription(newDto.getShortDescription());
        newEntity.setContent(newDto.getContent());
        newEntity.setThumbnail(newDto.getThumbnail());
        return newEntity;
    }
}
