package com.vnpost.demo.converter;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.entity.CategoryEntity;
import org.springframework.stereotype.Component;

@Component
public class CategoryConverter {

    public CategoryDto toDto(CategoryEntity categoryEntity){
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(categoryEntity.getId());
        categoryDto.setName(categoryEntity.getName());
        categoryDto.setGroupId(categoryEntity.getGroupCategory().getId());
        categoryDto.setGroupName(categoryEntity.getGroupCategory().getName());
        return categoryDto;
    }

    public CategoryEntity toEntity(CategoryDto categoryDto){
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setName(categoryDto.getName());
        categoryEntity.setGroupCategory(categoryEntity.getGroupCategory());
        return categoryEntity;
    }
}
