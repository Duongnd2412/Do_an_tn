package com.vnpost.demo.converter;

import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.entity.CommentEntity;
import org.springframework.stereotype.Component;

@Component
public class CommentConverter {
    public CommentDto toDto(CommentEntity commentEntity){
        CommentDto commentDto = new CommentDto();
        commentDto.setId(commentEntity.getId());
        commentDto.setComment(commentEntity.getComment());
        commentDto.setCreatedDate(commentEntity.getCreatedDate());
        commentDto.setName(commentEntity.getName());
        commentDto.setNewId(commentEntity.getNewEntity().getId());
        commentDto.setTitleNew(commentEntity.getNewEntity().getTitle());
        return commentDto;
    }
}
