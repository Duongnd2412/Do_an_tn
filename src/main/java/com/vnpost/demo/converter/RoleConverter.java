package com.vnpost.demo.converter;

import com.vnpost.demo.dto.RoleDto;
import com.vnpost.demo.entity.RoleEntity;
import com.vnpost.demo.entity.UserEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.stream.Collectors;


@Component
public class RoleConverter {

    public RoleDto toDto(Optional<RoleEntity> roleEntity){
        RoleDto roleDto = new RoleDto();
        roleDto.setId(roleEntity.get().getId());
        roleDto.setName(roleEntity.get().getName());
        roleDto.setGroupId(roleEntity.get().getGroupPmEntities().stream().map(permissionEntity -> permissionEntity.getId()).collect(Collectors.toList()));
        roleDto.setUserId(roleEntity.get().getUsers().stream().map(userEntity -> userEntity.getId()).collect(Collectors.toList()));
        return roleDto;
    }

}
