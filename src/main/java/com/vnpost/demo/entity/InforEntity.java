package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "infor")
@Getter
@Setter
public class InforEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "logo")
    private String logo;
    @Column(name = "email")
    private String email;
    @Column(name = "group_name")
    private String groupName;
    @Column(name="website_name")
    private String websiteName;
}
