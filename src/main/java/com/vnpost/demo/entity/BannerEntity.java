package com.vnpost.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "banner")
@Getter
@Setter
public class BannerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name1")
    private String name1;
    @Column(name = "name2")
    private String name2;
    @Column(name = "name3")
    private String name3;
    @Column(name = "banner1")
    private String banner1;
    @Column(name = "banner2")
    private String banner2;
    @Column(name = "banner3")
    private String banner3;

}
