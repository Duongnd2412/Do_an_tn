package com.vnpost.demo.controller.admin;

import com.vnpost.demo.dto.AbstractDto;
import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.sevice.FeedbackService;
import com.vnpost.demo.util.MessageUtil;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/admin/feedback")
public class FeedbackController {
    @Autowired
    private FeedbackService feedbackService;
    @Autowired
    private MessageUtil messageUtil;

    @GetMapping("/list")
    public ModelAndView showList(HttpServletRequest request,
                                 @RequestParam("page") int page,
                                 @RequestParam("limit") int limit){
        AbstractDto abstractDto = new CategoryDto();
        abstractDto.setPage(page);
        abstractDto.setLimit(limit);
        abstractDto.setTotalItem(feedbackService.getTotalItem());
        abstractDto.setTotalPage((int)Math.ceil((double) abstractDto.getTotalItem()/limit));
        Sort sort = Sort.by(Sort.Order.desc("id"));
        ModelAndView mav = new ModelAndView("admin/feedback");
        Pageable pageable = PageRequest.of(page-1,limit,sort);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("model",abstractDto);
        mav.addObject("feedback",feedbackService.findAll(pageable));
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

}
