package com.vnpost.demo.controller.admin;

import com.vnpost.demo.dto.BannerDto;
import com.vnpost.demo.dto.InforDto;
import com.vnpost.demo.dto.UserDto;
import com.vnpost.demo.repository.InforRepository;
import com.vnpost.demo.sevice.BannerService;
import com.vnpost.demo.sevice.InforService;
import com.vnpost.demo.util.MessageUtil;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("/admin/infor")
public class InforController {
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private InforService inforService;
    @Autowired
    private BannerService bannerService;

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id")long id, HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/infor");
        InforDto inforDto = inforService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("infors",inforDto);
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }

    @GetMapping("/edit-banner/{id}")
    public ModelAndView editBanner(@PathVariable("id")long id, HttpServletRequest request){
        ModelAndView mav = new ModelAndView("admin/banner");
        BannerDto bannerDto = bannerService.findById(id);
        if (request.getParameter("message") != null){
            Map<String , String> message = messageUtil.getMessage(request.getParameter("message"));
            mav.addObject("message",message.get("message"));
            mav.addObject("alert",message.get("alert"));
        }
        mav.addObject("banner",bannerDto);
        mav.addObject("roles", SecurityUtils.getPrincipal().getAuthorities());
        return mav;
    }
}
