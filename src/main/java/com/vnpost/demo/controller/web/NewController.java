package com.vnpost.demo.controller.web;

import com.vnpost.demo.dto.CategoryDto;
import com.vnpost.demo.dto.CommentDto;
import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.entity.NewEntity;
import com.vnpost.demo.sevice.*;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller(value = "webNew")
public class NewController {
    @Autowired
    private NewService newService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private GroupCategoryService groupService;
    @Autowired
    private InforService inforService;

    @GetMapping("/tin-tuc")
    public ModelAndView newPage(){
        ModelAndView mav = new ModelAndView("/web/news/index");
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("category",categoryService.findAll());
        mav.addObject("infor",inforService.findAll());
        return mav;
    }

    @GetMapping("/bai-viet/chi-tiet/{id}")
    public String getNew(Model model, @PathVariable("id")Long id){
        newService.countViews(id);
        NewDto newDto = newService.findById(id);
        CategoryDto categoryDto = categoryService.findAllNewById(newDto.getCategoryId());
        List<CommentDto> commentDtoList = newDto.getListComment();
        Collections.sort(commentDtoList, (o1, o2) -> o2.getId().compareTo(o1.getId()));
        model.addAttribute("group",groupService.findAllNew());
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("infor",inforService.findAll());
        model.addAttribute("itemNew",categoryDto.getListNew());
        model.addAttribute("news",newDto);
        model.addAttribute("comments",commentDtoList);
        return "web/news/new";
    }
    @GetMapping("/search-new")
    public ModelAndView search(@ModelAttribute NewDto newDto){
//        AbstractDto abstractDto = new NewDto();
//        abstractDto.setPage(page);
//        abstractDto.setLimit(limit);
//        abstractDto.setTotalItem(newService.getTotalItem());
//        abstractDto.setTotalPage((int)Math.ceil((double) abstractDto.getTotalItem()/limit));
//        Sort sort = Sort.by(Sort.Order.desc("id"));
        ModelAndView mav = new ModelAndView("web/news/new-search");
//        Pageable pageable = PageRequest.of(page-1,limit,sort);
        mav.addObject("newDto",newService.findAll(newDto));
//        mav.addObject("model",abstractDto);
        mav.addObject("title",newDto.getTitle());
        mav.addObject("category",categoryService.findAll());
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("infor",inforService.findAll());
        return mav;
    }
}
