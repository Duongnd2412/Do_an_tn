package com.vnpost.demo.controller.web;

import com.vnpost.demo.dto.NewDto;
import com.vnpost.demo.sevice.CategoryService;
import com.vnpost.demo.sevice.GroupCategoryService;
import com.vnpost.demo.sevice.InforService;
import com.vnpost.demo.sevice.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller(value = "webController")
@RequestMapping("/bai-viet")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private GroupCategoryService groupService;
    @Autowired
    private InforService inforService;

    @GetMapping("/the-loai/{id}")
    public String getNewCategory(@PathVariable("id")Long id, Model model){
        model.addAttribute("group",groupService.findAllNew());
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("groupId",categoryService.findAllNewById(id));
        model.addAttribute("infor",inforService.findAll());
        return "web/news/category";
    }
    @GetMapping("/nhom/the-loai/{id}")
    public String getNewGroupCate(@PathVariable("id")Long id, Model model){
        model.addAttribute("infor",inforService.findAll());
        model.addAttribute("group",groupService.findAllNew());
        model.addAttribute("groupId",groupService.findAllNewById(id));
        model.addAttribute("category",categoryService.findAll());
        return "web/news/category";
    }
}
