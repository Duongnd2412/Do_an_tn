package com.vnpost.demo.controller.web;

import com.vnpost.demo.dto.MyUser;
import com.vnpost.demo.sevice.*;
import com.vnpost.demo.util.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller(value = "web")
public class HomeController {

    @Autowired
    private GroupCategoryService groupService;
    @Autowired
    private InforService inforService;
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private BannerService bannerService;
    @Autowired
    private NewService newService;


    @GetMapping({"/trang-chu","/"})
    public ModelAndView homePage(){

        ModelAndView mav = new ModelAndView("web/home");
        Sort sort = Sort.by(Sort.Order.desc("id"));
        Sort sort1 = Sort.by(Sort.Order.desc("count"));
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("infor",inforService.findAll());
        mav.addObject("banner",bannerService.findAll());
        mav.addObject("category",categoryService.findAllNew());
        mav.addObject("news",newService.findAll(sort));
        mav.addObject("newCount",newService.findAll(sort1));
        return mav;
    }
    @GetMapping("/dang-nhap")
    public ModelAndView login(){
        ModelAndView mav = new ModelAndView("login");
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("infor",inforService.findAll());
        return mav;
    }


}
