package com.vnpost.demo.controller.web;

import com.vnpost.demo.sevice.GroupCategoryService;
import com.vnpost.demo.sevice.InforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class FeedBackWebController {
    @Autowired
    private GroupCategoryService groupService;
    @Autowired
    private InforService inforService;

    @GetMapping("/feedback")
    public ModelAndView feedback(){
        ModelAndView mav = new ModelAndView("web/feedback");
        mav.addObject("group",groupService.findAllNew());
        mav.addObject("infor",inforService.findAll());
        return mav;
    }
}
