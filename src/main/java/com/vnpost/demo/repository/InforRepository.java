package com.vnpost.demo.repository;

import com.vnpost.demo.entity.InforEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InforRepository extends JpaRepository<InforEntity,Long> {
}
