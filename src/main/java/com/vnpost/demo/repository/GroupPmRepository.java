package com.vnpost.demo.repository;

import com.vnpost.demo.entity.GroupPmEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupPmRepository extends JpaRepository<GroupPmEntity,Long> {
}
