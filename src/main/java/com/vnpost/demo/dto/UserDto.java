package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;

@Getter
@Setter
@ToString
public class UserDto extends AbstractDto{
    @Email(message = "email can't be null")
    private String email;
    private String password;
    private String confirmPassword;
    private String oldPassword;
    private String phone;
    private String gender;
    private String avatar;
    private String fullName;
    private String roleCode;
    private Long roleId;
    private String roleName;
    private String address;
    private int checkLogin;


}
