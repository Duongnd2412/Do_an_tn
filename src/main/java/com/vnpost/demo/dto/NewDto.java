package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class NewDto extends AbstractDto {
    @NotNull(message = "title can't be null")
    private String title;
    @NotNull(message = "thumbnail can't be null")
    private String thumbnail;
    @NotNull(message = "shortDescription can't be null")
    private String shortDescription;
    @NotNull(message = "content can't be null")
    private String content;
    private String categoryName;
    private Long categoryId;
    private Integer count;
    private Integer codeGroupCategory;
    List<Long> commentId = new ArrayList<>();
    List<CommentDto> listComment = new ArrayList<>();

}
