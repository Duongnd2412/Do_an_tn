package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;


@Getter
@Setter
public class FeedbackDto extends AbstractDto {
    private String email;
    private String content;
    private Date createdDate;
}
