package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommentDto extends AbstractDto {
    private String name;
    private String comment;
    private Long newId;
    private String titleNew;
}
