package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InforDto extends AbstractDto {
    private String name;
    private String logo;
    private String email;
    private String groupName;
    private String websiteName;
}
