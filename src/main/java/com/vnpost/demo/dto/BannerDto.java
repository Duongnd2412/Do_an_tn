package com.vnpost.demo.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BannerDto extends AbstractDto{
    private String name1;
    private String name2;
    private String name3;
    private String banner1;
    private String banner2;
    private String banner3;
}
